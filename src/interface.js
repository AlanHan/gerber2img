// Quentin Bolsee and Jake Read, MIT Center for Bits and Atoms, 2023
// import * as saveSvgAsPng from 'save-svg-as-png'
// import * as downloadAsPng from 'save-svg-as-png'
import process from "process";
import { Buffer } from "buffer";
import EventEmitter from "events";

window.Buffer = Buffer;
window.process = process;
window.EventEmitter = EventEmitter;

import { writeMetadataB } from 'png-metadata';
import gerberToSvg from 'gerber-to-svg';

let sourceElem = document.getElementById("body");
let svgElem = document.getElementById("previewSVG");


// order of known layers, KiCAD and EAGLE namings
const lookupOrder = [
    "Edge_Cuts", // KiCAD
    "profile",   // EAGLE
    "B_Cu",
    "copper_bottom",
    "F_Cu",
    "copper_top",
    "B_Mask",
    "soldermask_bottom",
    "F_Mask",
    "soldermask_top",
    "B_Paste",
    "solderpaste_bottom",
    "F_Paste",
    "solderpaste_top",
    "drill",
    "B_Silkscreen",
    "B_Silks",
    "silkscreen_bottom",
    "F_Silkscreen",
    "F_Silks",
    "silkscreen_top",
];

const lookupColor = {
    "Edge_Cuts": [70,105,58,1.0],
    "profile": [70,105,58,1.0],
    "B_Cu": [30,130,220,0.8],
    "copper_bottom": [30,130,220,0.8],
    "F_Cu": [89,165,82,0.8],
    "copper_top": [89,165,82,0.8],
    "B_Mask": [200,177,170,0.6],
    "soldermask_bottom": [200,177,170,0.6],
    "F_Mask": [239,177,58,1.0],
    "soldermask_top": [239,177,58,1.0],
    "B_Paste": [200,20,230,0.2],
    "solderpaste_bottom": [200,20,230,0.2],
    "F_Paste": [200,20,230,0.3],
    "solderpaste_top": [200,20,230,0.3],
    "drill": [120,120,120,1.0],
    "B_Silkscreen": [255,255,255,0.6],
    "B_Silks": [255,255,255,0.6],
    "silkscreen_bottom": [255,255,255,0.6],
    "F_Silkscreen": [255,255,255,1.0],
    "F_Silks": [255,255,255,1.0],
    "silkscreen_top": [255,255,255,1.0],
};

let globalLayers = [];
let globalLayersXML = [];
let globalXML = null;

// units in mm
let globalSettings = {
    fillEdge : true,
    transparent : false,
    blackAndWhite : false,
    asSVG: false,
    dpi: 1000,
    svgOrig : [NaN, NaN],
    lockOrig : false,
    svgDim : [NaN, NaN],
    lockDim : false,
    svgMargin : [0, 0],
};

function renderSVG() {
    if (globalXML === null) {
        svgElem.outerHTML = '<svg id="previewSVG"></svg>';
    } else {
        let serializer = new XMLSerializer();
        svgElem.outerHTML = serializer.serializeToString(globalXML);
    }
    // update reference
    svgElem = document.getElementById("previewSVG");
}

function updateSVG(fromSettingsChange=false) {
    let parser = new DOMParser();
    globalXML = parser.parseFromString('<svg id="previewSVG" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"></svg>', "text/xml");

    if (globalLayers.length == 0) {
        renderSVG();
        return;
    }

    let layersSorted = globalLayers.toSorted((a, b) => {
        return lookupOrder.indexOf(a[0]) - lookupOrder.indexOf(b[0]);
    });

    let layerName, layerTxt;

    let xMin = Number.MAX_VALUE;
    let xMax = -Number.MAX_VALUE;
    let yMin = Number.MAX_VALUE;
    let yMax = -Number.MAX_VALUE;

    let deltaY = [];

    // reset XML
    let globalXMLRoot = globalXML.childNodes[0];
    globalXMLRoot.setAttribute("stroke-linecap", "round");
    globalXMLRoot.setAttribute("stroke-linejoin", "round");
    globalXMLRoot.setAttribute("stroke-width", "0");
    // globalXMLRoot.setAttribute("background-color", "rgb(255,255,255)");
    globalXMLRoot.setAttribute("fill-rule", "evenodd");

    let layerVB, layerXML, layerXMLRoot;

    // compute viewBox
    for ([layerName, layerTxt] of layersSorted) {
        layerXML = parser.parseFromString(layerTxt, "text/xml");
        layerXMLRoot = layerXML.childNodes[0];
        layerVB = layerXMLRoot.getAttribute("viewBox").split(" ").map((x) => Number(x));
        if (layerXMLRoot.childNodes.length == 0) {
            continue;
        }
        xMin = Math.min(xMin, layerVB[0]);
        xMax = Math.max(xMax, layerVB[0]+layerVB[2]);
        yMin = Math.min(yMin, layerVB[1]);
        yMax = Math.max(yMax, layerVB[1]+layerVB[3]);
    }
    // sync settings
    if (!fromSettingsChange) {
        if (!globalSettings.lockOrig || isNaN(globalSettings.svgOrig[0])) {
            globalSettings.svgOrig[0] = xMin / 1000.0;
            globalSettings.svgOrig[1] = yMin / 1000.0;
        }
        if (!globalSettings.lockDim || isNaN(globalSettings.svgDim[0])) {
            globalSettings.svgDim[0] = (xMax - xMin) / 1000.0;
            globalSettings.svgDim[1] = (yMax - yMin) / 1000.0;
        }
    }
    writeSettings();
    let vb = [
        (globalSettings.svgOrig[0]-globalSettings.svgMargin[0]),
        (globalSettings.svgOrig[1]-globalSettings.svgMargin[1]),
        (globalSettings.svgDim[0]+2*globalSettings.svgMargin[0]),
        (globalSettings.svgDim[1]+2*globalSettings.svgMargin[1])
    ].map((x) => x * 1000);

    globalXMLRoot.setAttribute("viewBox", vb.join(' '));
    globalXMLRoot.setAttribute("width", `${(vb[2]/1000).toFixed(3)}mm`);
    globalXMLRoot.setAttribute("height", `${(vb[3]/1000).toFixed(3)}mm`);
    if (!globalSettings.transparent) {
        let backgroundElem = globalXMLRoot.ownerDocument.createElement("rect");
        if (globalSettings.blackAndWhite) {
            backgroundElem.setAttribute("fill", "rgb(0,0,0)");
        } else {
            backgroundElem.setAttribute("fill", "rgb(255,255,255)");
        }
        backgroundElem.setAttribute("x", vb[0]);
        backgroundElem.setAttribute("y", vb[1]);
        backgroundElem.setAttribute("width", vb[2]);
        backgroundElem.setAttribute("height", vb[3]);
        globalXMLRoot.appendChild(backgroundElem);
    }

    for ([layerName, layerTxt] of layersSorted) {
        layerXML = parser.parseFromString(layerTxt, "text/xml");
        layerXMLRoot = layerXML.childNodes[0];
        layerVB = layerXMLRoot.getAttribute("viewBox").split(" ").map((x) => Number(x));

        let layerRGB = 'rgb('+lookupColor[layerName].slice(0, 3).join(',')+')';
        let layerOpacity = lookupColor[layerName][3];

        for (let g of layerXMLRoot.childNodes) {
            if (g.tagName == "g") {
                const regex_translate = /translate\([-.\d]+,\s*([-.\d]+)\)/;
                const m = g.getAttribute("transform").match(regex_translate);
                // fix vertical alignment (none of this makes sense)
                let ty = Number(m[1]);
                ty += ((vb[1] + vb[3]) - (layerVB[1] + layerVB[3]));
                ty += (vb[1] - layerVB[1]);
                g.setAttribute("transform", `translate(0, ${ty}) scale(1, -1)`);

                // let ty = Number(m[1]) + (layerVB[3]-(yMax-yMin))/2;
                if (globalSettings.blackAndWhite) {
                    g.setAttribute("opacity", 1.0);
                    g.setAttribute("fill", "rgb(255,255,255)");
                    g.setAttribute("stroke", "rgb(255,255,255)");
                } else {
                    g.setAttribute("opacity", layerOpacity);
                    g.setAttribute("fill", layerRGB);
                    g.setAttribute("stroke", layerRGB);
                }
                g.childNodes.forEach((p) => {
                    if (["profile", "Edge_Cuts"].includes(layerName) && globalSettings.fillEdge) {
                        // fill mode
                        p.removeAttribute("fill");
                        p.setAttribute("stroke", "none");
                    }
                });
            }
            globalXMLRoot.appendChild(g.cloneNode(true));
        }
    }

    renderSVG();
}

function setDPI(canvas, dpi) {
    // Resize canvas and scale future draws.
    var scaleFactor = dpi / 96;
    canvas.width = Math.ceil(canvas.width * scaleFactor);
    canvas.height = Math.ceil(canvas.height * scaleFactor);
    var ctx = canvas.getContext('2d');
    ctx.scale(scaleFactor, scaleFactor);
}

function downloadBlob(blob, name = 'file.txt') {
    if (
      window.navigator &&
      window.navigator.msSaveOrOpenBlob
    ) return window.navigator.msSaveOrOpenBlob(blob);

    // For other browsers:
    // Create a link pointing to the ObjectURL containing the blob.
    const data = window.URL.createObjectURL(blob);

    const link = document.createElement('a');
    link.href = data;
    link.download = name;

    // this is necessary as link.click() does not work on the latest firefox
    link.dispatchEvent(
      new MouseEvent('click', {
        bubbles: true,
        cancelable: true,
        view: window
      })
    );

    setTimeout(() => {
      // For Firefox it is necessary to delay revoking the ObjectURL
      window.URL.revokeObjectURL(data);
      link.remove();
    }, 100);
}

async function downloadImage(separateLayers=false) {
    if (separateLayers) {
        return;
    }

    let svgTxt = new XMLSerializer().serializeToString(svgElem);
    let svgURL = "data:image/svg+xml;charset=utf-8,"+encodeURIComponent(svgTxt);

    if (globalSettings.asSVG) {
        let downloadLink = document.createElement("a");
        downloadLink.href = svgURL;
        downloadLink.download = "render.svg";
        document.body.appendChild(downloadLink);
        downloadLink.click();
        document.body.removeChild(downloadLink);
    } else {
        let scaleFactor = globalSettings.dpi / 96;

        let image = new Image();
        let canvas = document.getElementById('canvas');
        let mmWidth = globalSettings.svgDim[0] + 2*globalSettings.svgMargin[0];
        let mmHeight = globalSettings.svgDim[1] + 2*globalSettings.svgMargin[1];
        let pxWidth = Math.floor(globalSettings.dpi * mmWidth / 25.4);
        let pxHeight = Math.floor(globalSettings.dpi * mmHeight / 25.4);

        // dummy resolution
        canvas.width = pxWidth;
        canvas.height = pxHeight;
        let ctx = canvas.getContext('2d', { alpha: globalSettings.transparent });
        ctx.scale(scaleFactor, scaleFactor);
        image.addEventListener('load', async (e) => {
            // fix transparent glitch
            // ctx.fill =
            // ctx.rect(0, 0, e.target.width, e.target.height);
            // ctx.fill();
            ctx.drawImage(e.target, 0, 0, e.target.width, e.target.height);
            canvas.toBlob(async (blob) => {
                let metadata = {
                    pHYs: {
                        x: Math.round(1000 * globalSettings.dpi / 25.4),
                        y: Math.round(1000 * globalSettings.dpi / 25.4),
                        units: 1 // inches
                    },
                    tExt: {
                        Software: "gerber2img"
                    }
                }
                let newBlob = await writeMetadataB(blob, metadata);
                downloadBlob(newBlob, "render.png");
                // saveAs(newBlob, title);
            });
            // writeMetadataB()
            // const a = document.createElement('a');
            // a.style.display = 'none';
            // a.href = canvas.toDataURL("image/png");
            // a.download = 'render.png';
            // document.body.appendChild(a);
            // a.click();
        });
        image.src = svgURL;
    }
}

var dropZone = document.getElementById('dropZone');

function showDropZone() {
    dropZone.style.visibility = "visible";
}

function hideDropZone() {
    dropZone.style.visibility = "hidden";
}

window.addEventListener('dragenter', (e) => {
    showDropZone();
});

function allowDrag(e) {
    if (true) {  // Test that the item being dragged is a valid one
        e.dataTransfer.dropEffect = 'copy';
        e.preventDefault();
    }
}

dropZone.addEventListener('dragenter', allowDrag);
dropZone.addEventListener('dragover', allowDrag);

dropZone.addEventListener('dragleave', (e) => {
    hideDropZone();
});

dropZone.addEventListener('drop', (e) => {
    e.preventDefault();
    hideDropZone();
    if (!e.dataTransfer.items) {
        return;
    }

    // layerName: SVG
    let promiseAllSVG = [];

    for (let file of e.dataTransfer.files) {
        let layerName;
        if (file.name.endsWith('.xln') || file.name.endsWith('.drl')) {
            layerName = "drill";
        } else {
            if (!file.name.endsWith('.gbr')) {
                continue;
            }
            let filename = file.name.replace('.gbr', '');
            let layerNum = -1;

            for (let j in lookupOrder) {
                layerName = lookupOrder[j];
                if (filename.endsWith(layerName)) {
                    layerNum = j;
                    break;
                }
            }
            if (layerNum == -1) {
                continue;
            }
        }

        let promiseSVG = new Promise((resolve, reject) => {
            const reader = new FileReader();
            reader.onload = (event) => {
                const options = {
                    encoding: 'utf8',
                    optimizePaths: true
                };
                gerberToSvg(reader.result, options, (err, svg) => {
                    if (err) {
                        resolve(null);
                    } else {
                        resolve([layerName, svg]);
                    }
                });
            }
            reader.onerror = reject;
            reader.readAsText(file);
        });
        promiseAllSVG.push(promiseSVG);
    }
    Promise.all(promiseAllSVG).then((values) => {
        globalLayers = values;
        updateSVG();
    })
});

function readSettings() {
    let settingsFill = document.getElementById("settingsFill");
    let settingsBW = document.getElementById("settingsBW");
    let settingsTransparent = document.getElementById("settingsTransparent");
    let settingsAsSVG = document.getElementById("settingsAsSVG");
    let settingsDPI = document.getElementById("settingsDPI");
    let settingsOrigX = document.getElementById("settingsOrigX");
    let settingsOrigY = document.getElementById("settingsOrigY");
    let settingsDimX = document.getElementById("settingsDimX");
    let settingsDimY = document.getElementById("settingsDimY");
    let settingsMarginX = document.getElementById("settingsMarginX");
    let settingsMarginY = document.getElementById("settingsMarginY");

    globalSettings.fillEdge = settingsFill.checked;
    globalSettings.blackAndWhite = settingsBW.checked;
    globalSettings.transparent = settingsTransparent.checked;
    globalSettings.asSVG = settingsAsSVG.checked;
    globalSettings.dpi = Number(settingsDPI.value);
    globalSettings.svgMargin = [
        Number(settingsMarginX.value),
        Number(settingsMarginY.value)
    ]
    globalSettings.svgOrig = [
        Number(settingsOrigX.value),
        Number(settingsOrigY.value)
    ];
    globalSettings.svgDim = [
        Number(settingsDimX.value),
        Number(settingsDimY.value)
    ];
}

function writeSettings() {
    let settingsOrigX = document.getElementById("settingsOrigX");
    let settingsOrigY = document.getElementById("settingsOrigY");
    let settingsDimX = document.getElementById("settingsDimX");
    let settingsDimY = document.getElementById("settingsDimY");

    settingsOrigX.value = globalSettings.svgOrig[0];
    settingsOrigY.value = globalSettings.svgOrig[1];
    settingsDimX.value = globalSettings.svgDim[0];
    settingsDimY.value = globalSettings.svgDim[1];
}

/* buttons */
function initListeners() {
    // document.getElementById("loadButton").addEventListener("click", () => {
    //     document.getElementById("fileInput").click();
    // });

    for (let elementId of [
        'settingsFill',
        'settingsAsSVG',
        'settingsBW',
        'settingsTransparent',
        'settingsDPI',
        'settingsOrigX',
        'settingsOrigY',
        'settingsDimX',
        'settingsDimY',
        'settingsMarginX',
        'settingsMarginY']) {
        document.getElementById(elementId).addEventListener("change", () => {
            readSettings();
            updateSVG(true);
        })
    }
}

let lockOrigElem = document.getElementById("settingsLockOrig");
let lockDimElem = document.getElementById("settingsLockDim");

lockOrigElem.addEventListener("click", () => {
    let settingsOrigX = document.getElementById("settingsOrigX");
    let settingsOrigY = document.getElementById("settingsOrigY");
    if (globalSettings.lockOrig) {
        settingsOrigX.disabled = false;
        settingsOrigY.disabled = false;
        globalSettings.lockOrig = false;
        lockOrigElem.innerHTML = "🔓";
    } else {
        settingsOrigX.disabled = true;
        settingsOrigY.disabled = true;
        globalSettings.lockOrig = true;
        lockOrigElem.innerHTML = "🔒";
    }
});

lockDimElem.addEventListener("click", () => {
    let settingsDimX = document.getElementById("settingsDimX");
    let settingsDimY = document.getElementById("settingsDimY");
    if (globalSettings.lockDim) {
        settingsDimX.disabled = false;
        settingsDimY.disabled = false;
        globalSettings.lockDim = false;
        lockDimElem.innerHTML = "🔓";
    } else {
        settingsDimX.disabled = true;
        settingsDimY.disabled = true;
        globalSettings.lockDim = true;
        lockDimElem.innerHTML = "🔒";
    }
});

document.getElementById("downloadRenderButton").addEventListener("click", async () => {
    await downloadImage(false);
});

// document.getElementById("downloadlayersButton").addEventListener("click", () => {
//     downloadImage(true);
// });

initListeners();
readSettings();

globalLayers = JSON.parse('[["Edge_Cuts","<svg version=\\"1.1\\" xmlns=\\"http://www.w3.org/2000/svg\\" xmlns:xlink=\\"http://www.w3.org/1999/xlink\\" stroke-linecap=\\"round\\" stroke-linejoin=\\"round\\" stroke-width=\\"0\\" fill-rule=\\"evenodd\\" width=\\"34.39mm\\" height=\\"38.835mm\\" viewBox=\\"52655 -71170 34390 38835\\"><g transform=\\"translate(0,-103505) scale(1,-1)\\" fill=\\"currentColor\\" stroke=\\"currentColor\\"><path d=\\"M 86995 -34925 A 2540 2540 0 0 1 84455 -32385 L 55245 -32385 A 2540 2540 0 0 1 52705 -34925 L 52705 -68580 A 2540 2540 0 0 1 55245 -71120 L 84455 -71120 A 2540 2540 0 0 1 86995 -68580 L 86995 -34925\\" fill=\\"none\\" stroke-width=\\"100\\"/></g></svg>"],["F_Cu","<svg version=\\"1.1\\" xmlns=\\"http://www.w3.org/2000/svg\\" xmlns:xlink=\\"http://www.w3.org/1999/xlink\\" stroke-linecap=\\"round\\" stroke-linejoin=\\"round\\" stroke-width=\\"0\\" fill-rule=\\"evenodd\\" width=\\"32.531mm\\" height=\\"34.7mm\\" viewBox=\\"53775 -68780 32531 34700\\"><defs><rect x=\\"-635\\" y=\\"-1270\\" width=\\"1270\\" height=\\"2540\\" id=\\"uZojfCN5iBEs_pad-10\\"/><rect x=\\"-850\\" y=\\"-1000\\" width=\\"1700\\" height=\\"2000\\" id=\\"uZojfCN5iBEs_pad-11\\"/><rect x=\\"-1000\\" y=\\"-850\\" width=\\"2000\\" height=\\"1700\\" id=\\"uZojfCN5iBEs_pad-12\\"/><rect x=\\"-1500\\" y=\\"-800\\" width=\\"3000\\" height=\\"1600\\" id=\\"uZojfCN5iBEs_pad-13\\"/><rect x=\\"-600\\" y=\\"-1200\\" width=\\"1200\\" height=\\"2400\\" id=\\"uZojfCN5iBEs_pad-14\\"/><circle cx=\\"0\\" cy=\\"0\\" r=\\"600\\" id=\\"uZojfCN5iBEs_pad-15\\"/><rect x=\\"-370\\" y=\\"-1200\\" width=\\"740\\" height=\\"2400\\" id=\\"uZojfCN5iBEs_pad-16\\"/></defs><g transform=\\"translate(0,-102860) scale(1,-1)\\" fill=\\"currentColor\\" stroke=\\"currentColor\\"><use xlink:href=\\"#uZojfCN5iBEs_pad-10\\" x=\\"67970\\" y=\\"-61550\\"/><use xlink:href=\\"#uZojfCN5iBEs_pad-10\\" x=\\"65430\\" y=\\"-61550\\"/><use xlink:href=\\"#uZojfCN5iBEs_pad-10\\" x=\\"62890\\" y=\\"-61550\\"/><use xlink:href=\\"#uZojfCN5iBEs_pad-10\\" x=\\"60350\\" y=\\"-61550\\"/><use xlink:href=\\"#uZojfCN5iBEs_pad-10\\" x=\\"57810\\" y=\\"-61550\\"/><use xlink:href=\\"#uZojfCN5iBEs_pad-10\\" x=\\"55270\\" y=\\"-61550\\"/><use xlink:href=\\"#uZojfCN5iBEs_pad-11\\" x=\\"82550\\" y=\\"-48800\\"/><use xlink:href=\\"#uZojfCN5iBEs_pad-11\\" x=\\"82550\\" y=\\"-52800\\"/><use xlink:href=\\"#uZojfCN5iBEs_pad-12\\" x=\\"81375\\" y=\\"-55880\\"/><use xlink:href=\\"#uZojfCN5iBEs_pad-12\\" x=\\"77375\\" y=\\"-55880\\"/><use xlink:href=\\"#uZojfCN5iBEs_pad-12\\" x=\\"63595\\" y=\\"-55880\\"/><use xlink:href=\\"#uZojfCN5iBEs_pad-12\\" x=\\"59595\\" y=\\"-55880\\"/><use xlink:href=\\"#uZojfCN5iBEs_pad-12\\" x=\\"71215\\" y=\\"-55880\\"/><use xlink:href=\\"#uZojfCN5iBEs_pad-12\\" x=\\"67215\\" y=\\"-55880\\"/><use xlink:href=\\"#uZojfCN5iBEs_pad-13\\" x=\\"61235\\" y=\\"-35560\\"/><use xlink:href=\\"#uZojfCN5iBEs_pad-13\\" x=\\"61235\\" y=\\"-38100\\"/><use xlink:href=\\"#uZojfCN5iBEs_pad-13\\" x=\\"61235\\" y=\\"-40640\\"/><use xlink:href=\\"#uZojfCN5iBEs_pad-13\\" x=\\"61235\\" y=\\"-43180\\"/><use xlink:href=\\"#uZojfCN5iBEs_pad-13\\" x=\\"61235\\" y=\\"-45720\\"/><use xlink:href=\\"#uZojfCN5iBEs_pad-13\\" x=\\"61235\\" y=\\"-48260\\"/><use xlink:href=\\"#uZojfCN5iBEs_pad-13\\" x=\\"61235\\" y=\\"-50800\\"/><use xlink:href=\\"#uZojfCN5iBEs_pad-13\\" x=\\"78470\\" y=\\"-50800\\"/><use xlink:href=\\"#uZojfCN5iBEs_pad-13\\" x=\\"78470\\" y=\\"-48260\\"/><use xlink:href=\\"#uZojfCN5iBEs_pad-13\\" x=\\"78470\\" y=\\"-45720\\"/><use xlink:href=\\"#uZojfCN5iBEs_pad-13\\" x=\\"78470\\" y=\\"-43180\\"/><use xlink:href=\\"#uZojfCN5iBEs_pad-13\\" x=\\"78470\\" y=\\"-40640\\"/><use xlink:href=\\"#uZojfCN5iBEs_pad-13\\" x=\\"78470\\" y=\\"-38100\\"/><use xlink:href=\\"#uZojfCN5iBEs_pad-13\\" x=\\"78470\\" y=\\"-35560\\"/><use xlink:href=\\"#uZojfCN5iBEs_pad-14\\" x=\\"71125\\" y=\\"-52070\\"/><use xlink:href=\\"#uZojfCN5iBEs_pad-14\\" x=\\"68585\\" y=\\"-52070\\"/><use xlink:href=\\"#uZojfCN5iBEs_pad-15\\" x=\\"68580\\" y=\\"-34680\\"/><use xlink:href=\\"#uZojfCN5iBEs_pad-15\\" x=\\"68580\\" y=\\"-37220\\"/><use xlink:href=\\"#uZojfCN5iBEs_pad-15\\" x=\\"71120\\" y=\\"-37220\\"/><use xlink:href=\\"#uZojfCN5iBEs_pad-15\\" x=\\"71120\\" y=\\"-34680\\"/><use xlink:href=\\"#uZojfCN5iBEs_pad-11\\" x=\\"57785\\" y=\\"-47720\\"/><use xlink:href=\\"#uZojfCN5iBEs_pad-11\\" x=\\"57785\\" y=\\"-43720\\"/><use xlink:href=\\"#uZojfCN5iBEs_pad-16\\" x=\\"81915\\" y=\\"-61550\\"/><use xlink:href=\\"#uZojfCN5iBEs_pad-16\\" x=\\"81915\\" y=\\"-65450\\"/><use xlink:href=\\"#uZojfCN5iBEs_pad-16\\" x=\\"80645\\" y=\\"-61550\\"/><use xlink:href=\\"#uZojfCN5iBEs_pad-16\\" x=\\"80645\\" y=\\"-65450\\"/><use xlink:href=\\"#uZojfCN5iBEs_pad-16\\" x=\\"79375\\" y=\\"-61550\\"/><use xlink:href=\\"#uZojfCN5iBEs_pad-16\\" x=\\"79375\\" y=\\"-65450\\"/><use xlink:href=\\"#uZojfCN5iBEs_pad-16\\" x=\\"78105\\" y=\\"-61550\\"/><use xlink:href=\\"#uZojfCN5iBEs_pad-16\\" x=\\"78105\\" y=\\"-65450\\"/><use xlink:href=\\"#uZojfCN5iBEs_pad-16\\" x=\\"76835\\" y=\\"-61550\\"/><use xlink:href=\\"#uZojfCN5iBEs_pad-16\\" x=\\"76835\\" y=\\"-65450\\"/><path d=\\"M 77765 -67310 78105 -66970 78105 -65450 79375 -65450 79375 -67050 79375 -66720 M 79375 -67050 79635 -67310 81280 -67310 81915 -66675 81915 -65450 83140 -65450 85090 -63500 85090 -48895 84995 -48800 82550 -48800 M 85090 -48895 85090 -42545 80645 -38100 77470 -38100 79450 -38100 M 56515 -67310 71120 -67310 77765 -67310 M 71215 -67215 71120 -67310 M 55270 -60915 55270 -61550 55270 -66065 56515 -67310 M 71215 -55880 71215 -67215 M 81915 -68580 86106 -64389 86106 -41910 79756 -35560 77470 -35560 79450 -35560 M 60039.002 -59404.002 54260.998 -59404.002 53975 -59690 53975 -66675 55880 -68580 81915 -68580 M 60350 -59715 60039.002 -59404.002 M 60350 -61550 60350 -59715 M 81915 -59690 81915 -61550 M 77470 -48260 74930 -50800 74930 -56504.295 76845.206 -58419.501 80644.501 -58419.501 81915 -59690 M 74120.499 -49069.501 74120.499 -56839.602 76509.9 -59229.002 77384.002 -59229.002 78105 -59950 78105 -61550 M 77470 -45720 74120.499 -49069.501 M 76835 -60960 73310.998 -57435.998 73310.998 -47339.002 77470 -43180 M 76835 -61550 76835 -60960 M 56515 -56385.488 57914.512 -57785 62300 -57785 65430 -60915 M 56515 -48990 56515 -56385.488 M 57785 -47720 56515 -48990 M 57229.704 -58245 57229 -58245 55372 -56388 55372 -43053 57785 -40640 62235 -40640 M 61769.501 -58594.501 57579.205 -58594.501 57229.704 -58245 M 62890 -59715 61769.501 -58594.501 M 62890 -60915 62890 -59715 M 62235 -48392 62235 -48260 M 58325 -43180 57785 -43720 M 62235 -43180 58325 -43180 M 59595 -51530 59595 -55880 M 60325 -50800 59595 -51530 M 62235 -50800 60325 -50800 M 63595 -55880 67215 -55880 M 77375 -50895 77375 -55880 M 79375 -50800 77470 -50800 77375 -50895 M 81375 -55880 81915 -55880 82550 -55245 82550 -52800\\" fill=\\"none\\" stroke-width=\\"400\\"/></g></svg>"],["F_Mask","<svg version=\\"1.1\\" xmlns=\\"http://www.w3.org/2000/svg\\" xmlns:xlink=\\"http://www.w3.org/1999/xlink\\" stroke-linecap=\\"round\\" stroke-linejoin=\\"round\\" stroke-width=\\"0\\" fill-rule=\\"evenodd\\" width=\\"28.765mm\\" height=\\"32.57mm\\" viewBox=\\"54635 -66650 28765 32570\\"><defs><rect x=\\"-635\\" y=\\"-1270\\" width=\\"1270\\" height=\\"2540\\" id=\\"rjjz4qFsGmhm_pad-10\\"/><rect x=\\"-850\\" y=\\"-1000\\" width=\\"1700\\" height=\\"2000\\" id=\\"rjjz4qFsGmhm_pad-11\\"/><rect x=\\"-1000\\" y=\\"-850\\" width=\\"2000\\" height=\\"1700\\" id=\\"rjjz4qFsGmhm_pad-12\\"/><rect x=\\"-1500\\" y=\\"-800\\" width=\\"3000\\" height=\\"1600\\" id=\\"rjjz4qFsGmhm_pad-13\\"/><rect x=\\"-600\\" y=\\"-1200\\" width=\\"1200\\" height=\\"2400\\" id=\\"rjjz4qFsGmhm_pad-14\\"/><circle cx=\\"0\\" cy=\\"0\\" r=\\"600\\" id=\\"rjjz4qFsGmhm_pad-15\\"/><rect x=\\"-370\\" y=\\"-1200\\" width=\\"740\\" height=\\"2400\\" id=\\"rjjz4qFsGmhm_pad-16\\"/></defs><g transform=\\"translate(0,-100730) scale(1,-1)\\" fill=\\"currentColor\\" stroke=\\"currentColor\\"><use xlink:href=\\"#rjjz4qFsGmhm_pad-10\\" x=\\"67970\\" y=\\"-61550\\"/><use xlink:href=\\"#rjjz4qFsGmhm_pad-10\\" x=\\"65430\\" y=\\"-61550\\"/><use xlink:href=\\"#rjjz4qFsGmhm_pad-10\\" x=\\"62890\\" y=\\"-61550\\"/><use xlink:href=\\"#rjjz4qFsGmhm_pad-10\\" x=\\"60350\\" y=\\"-61550\\"/><use xlink:href=\\"#rjjz4qFsGmhm_pad-10\\" x=\\"57810\\" y=\\"-61550\\"/><use xlink:href=\\"#rjjz4qFsGmhm_pad-10\\" x=\\"55270\\" y=\\"-61550\\"/><use xlink:href=\\"#rjjz4qFsGmhm_pad-11\\" x=\\"82550\\" y=\\"-48800\\"/><use xlink:href=\\"#rjjz4qFsGmhm_pad-11\\" x=\\"82550\\" y=\\"-52800\\"/><use xlink:href=\\"#rjjz4qFsGmhm_pad-12\\" x=\\"81375\\" y=\\"-55880\\"/><use xlink:href=\\"#rjjz4qFsGmhm_pad-12\\" x=\\"77375\\" y=\\"-55880\\"/><use xlink:href=\\"#rjjz4qFsGmhm_pad-12\\" x=\\"63595\\" y=\\"-55880\\"/><use xlink:href=\\"#rjjz4qFsGmhm_pad-12\\" x=\\"59595\\" y=\\"-55880\\"/><use xlink:href=\\"#rjjz4qFsGmhm_pad-12\\" x=\\"71215\\" y=\\"-55880\\"/><use xlink:href=\\"#rjjz4qFsGmhm_pad-12\\" x=\\"67215\\" y=\\"-55880\\"/><use xlink:href=\\"#rjjz4qFsGmhm_pad-13\\" x=\\"61235\\" y=\\"-35560\\"/><use xlink:href=\\"#rjjz4qFsGmhm_pad-13\\" x=\\"61235\\" y=\\"-38100\\"/><use xlink:href=\\"#rjjz4qFsGmhm_pad-13\\" x=\\"61235\\" y=\\"-40640\\"/><use xlink:href=\\"#rjjz4qFsGmhm_pad-13\\" x=\\"61235\\" y=\\"-43180\\"/><use xlink:href=\\"#rjjz4qFsGmhm_pad-13\\" x=\\"61235\\" y=\\"-45720\\"/><use xlink:href=\\"#rjjz4qFsGmhm_pad-13\\" x=\\"61235\\" y=\\"-48260\\"/><use xlink:href=\\"#rjjz4qFsGmhm_pad-13\\" x=\\"61235\\" y=\\"-50800\\"/><use xlink:href=\\"#rjjz4qFsGmhm_pad-13\\" x=\\"78470\\" y=\\"-50800\\"/><use xlink:href=\\"#rjjz4qFsGmhm_pad-13\\" x=\\"78470\\" y=\\"-48260\\"/><use xlink:href=\\"#rjjz4qFsGmhm_pad-13\\" x=\\"78470\\" y=\\"-45720\\"/><use xlink:href=\\"#rjjz4qFsGmhm_pad-13\\" x=\\"78470\\" y=\\"-43180\\"/><use xlink:href=\\"#rjjz4qFsGmhm_pad-13\\" x=\\"78470\\" y=\\"-40640\\"/><use xlink:href=\\"#rjjz4qFsGmhm_pad-13\\" x=\\"78470\\" y=\\"-38100\\"/><use xlink:href=\\"#rjjz4qFsGmhm_pad-13\\" x=\\"78470\\" y=\\"-35560\\"/><use xlink:href=\\"#rjjz4qFsGmhm_pad-14\\" x=\\"71125\\" y=\\"-52070\\"/><use xlink:href=\\"#rjjz4qFsGmhm_pad-14\\" x=\\"68585\\" y=\\"-52070\\"/><use xlink:href=\\"#rjjz4qFsGmhm_pad-15\\" x=\\"68580\\" y=\\"-34680\\"/><use xlink:href=\\"#rjjz4qFsGmhm_pad-15\\" x=\\"68580\\" y=\\"-37220\\"/><use xlink:href=\\"#rjjz4qFsGmhm_pad-15\\" x=\\"71120\\" y=\\"-37220\\"/><use xlink:href=\\"#rjjz4qFsGmhm_pad-15\\" x=\\"71120\\" y=\\"-34680\\"/><use xlink:href=\\"#rjjz4qFsGmhm_pad-11\\" x=\\"57785\\" y=\\"-47720\\"/><use xlink:href=\\"#rjjz4qFsGmhm_pad-11\\" x=\\"57785\\" y=\\"-43720\\"/><use xlink:href=\\"#rjjz4qFsGmhm_pad-16\\" x=\\"81915\\" y=\\"-61550\\"/><use xlink:href=\\"#rjjz4qFsGmhm_pad-16\\" x=\\"81915\\" y=\\"-65450\\"/><use xlink:href=\\"#rjjz4qFsGmhm_pad-16\\" x=\\"80645\\" y=\\"-61550\\"/><use xlink:href=\\"#rjjz4qFsGmhm_pad-16\\" x=\\"80645\\" y=\\"-65450\\"/><use xlink:href=\\"#rjjz4qFsGmhm_pad-16\\" x=\\"79375\\" y=\\"-61550\\"/><use xlink:href=\\"#rjjz4qFsGmhm_pad-16\\" x=\\"79375\\" y=\\"-65450\\"/><use xlink:href=\\"#rjjz4qFsGmhm_pad-16\\" x=\\"78105\\" y=\\"-61550\\"/><use xlink:href=\\"#rjjz4qFsGmhm_pad-16\\" x=\\"78105\\" y=\\"-65450\\"/><use xlink:href=\\"#rjjz4qFsGmhm_pad-16\\" x=\\"76835\\" y=\\"-61550\\"/><use xlink:href=\\"#rjjz4qFsGmhm_pad-16\\" x=\\"76835\\" y=\\"-65450\\"/></g></svg>"],["F_Silkscreen","<svg version=\\"1.1\\" xmlns=\\"http://www.w3.org/2000/svg\\" xmlns:xlink=\\"http://www.w3.org/1999/xlink\\" stroke-linecap=\\"round\\" stroke-linejoin=\\"round\\" stroke-width=\\"0\\" fill-rule=\\"evenodd\\" width=\\"31.927400000000002mm\\" height=\\"41.975mm\\" viewBox=\\"53940 -73040 31927.4 41975\\"><g transform=\\"translate(0,-104105) scale(1,-1)\\" fill=\\"currentColor\\" stroke=\\"currentColor\\"><path d=\\"M 83026.2 -34999.533 83059.533 -34932.866 83126.2 -34866.2 83226.2 -34766.2 83259.533 -34699.533 83259.533 -34632.866 M 83192.866 -34932.866 83126.2 -34866.2 83092.866 -34799.533 83092.866 -34666.2 83126.2 -34599.533 83192.866 -34532.866 83326.2 -34499.533 83559.533 -34499.533 83692.866 -34532.866 83759.533 -34599.533 83792.866 -34666.2 83792.866 -34799.533 83759.533 -34866.2 83692.866 -34932.866 83559.533 -34966.2 83326.2 -34966.2 83192.866 -34932.866 M 83559.533 -35566.199 83092.866 -35566.199 M 83559.533 -35266.199 83192.866 -35266.199 83126.2 -35299.533 83092.866 -35366.199 83092.866 -35466.199 83126.2 -35532.866 83159.533 -35566.199 M 83126.2 -36166.199 83092.866 -36099.532 83092.866 -35966.199 83126.2 -35899.532 83192.866 -35866.199 83459.533 -35866.199 83526.2 -35899.532 83559.533 -35966.199 83559.533 -36099.532 83526.2 -36166.199 83459.533 -36199.532 83392.866 -36199.532 83326.2 -35866.199 M 83559.533 -36499.532 83092.866 -36499.532 M 83492.866 -36499.532 83526.2 -36532.866 83559.533 -36599.532 83559.533 -36699.532 83526.2 -36766.199 83459.533 -36799.532 83092.866 -36799.532 M 83559.533 -37032.865 83559.533 -37299.532 M 83792.866 -37132.865 83192.866 -37132.865 83126.2 -37166.199 83092.866 -37232.865 83092.866 -37299.532 M 83092.866 -37532.865 83559.533 -37532.865 M 83792.866 -37532.865 83759.533 -37499.532 83726.2 -37532.865 83759.533 -37566.199 83792.866 -37532.865 83726.2 -37532.865 M 83559.533 -37866.198 83092.866 -37866.198 M 83492.866 -37866.198 83526.2 -37899.532 83559.533 -37966.198 83559.533 -38066.198 83526.2 -38132.865 83459.533 -38166.198 83092.866 -38166.198 M 83459.533 -39266.198 83426.2 -39366.198 83392.866 -39399.531 83326.2 -39432.864 83226.2 -39432.864 83159.533 -39399.531 83126.2 -39366.198 83092.866 -39299.531 83092.866 -39032.864 83792.866 -39032.864 83792.866 -39266.198 83759.533 -39332.864 83726.2 -39366.198 83659.533 -39399.531 83592.866 -39399.531 83526.2 -39366.198 83492.866 -39332.864 83459.533 -39266.198 83459.533 -39032.864 M 83092.866 -39832.864 83126.2 -39766.198 83159.533 -39732.864 83226.2 -39699.531 83426.2 -39699.531 83492.866 -39732.864 83526.2 -39766.198 83559.533 -39832.864 83559.533 -39932.864 83526.2 -39999.531 83492.866 -40032.864 83426.2 -40066.198 83226.2 -40066.198 83159.533 -40032.864 83126.2 -39999.531 83092.866 -39932.864 83092.866 -39832.864 M 83092.866 -40466.197 83126.2 -40399.531 83192.866 -40366.197 83792.866 -40366.197 M 83126.2 -40699.531 83092.866 -40766.198 83092.866 -40899.531 83126.2 -40966.198 83192.866 -40999.531 83226.2 -40999.531 83292.866 -40966.198 83326.2 -40899.531 83326.2 -40799.531 83359.533 -40732.864 83426.2 -40699.531 83459.533 -40699.531 83526.2 -40732.864 83559.533 -40799.531 83559.533 -40899.531 83526.2 -40966.198 M 83126.2 -41566.198 83092.866 -41499.531 83092.866 -41366.198 83126.2 -41299.531 83192.866 -41266.198 83459.533 -41266.198 83526.2 -41299.531 83559.533 -41366.198 83559.533 -41499.531 83526.2 -41566.198 83459.533 -41599.531 83392.866 -41599.531 83326.2 -41266.198 M 83826.2 -41499.531 83726.2 -41399.531 M 83126.2 -42166.198 83092.866 -42099.531 83092.866 -41966.198 83126.2 -41899.531 83192.866 -41866.198 83459.533 -41866.198 83526.2 -41899.531 83559.533 -41966.198 83559.533 -42099.531 83526.2 -42166.198 83459.533 -42199.531 83392.866 -42199.531 83326.2 -41866.198 M 83726.2 -42999.531 83759.533 -43032.864 83792.866 -43099.531 83792.866 -43266.198 83759.533 -43332.864 83726.2 -43366.198 83659.533 -43399.531 83592.866 -43399.531 83492.866 -43366.198 83092.866 -42966.198 83092.866 -43399.531 M 83792.866 -43832.865 83792.866 -43899.531 83759.533 -43966.198 83726.2 -43999.531 83659.533 -44032.865 83526.2 -44066.198 83359.533 -44066.198 83226.2 -44032.865 83159.533 -43999.531 83126.2 -43966.198 83092.866 -43899.531 83092.866 -43832.865 83126.2 -43766.198 83159.533 -43732.865 83226.2 -43699.531 83359.533 -43666.198 83526.2 -43666.198 83659.533 -43699.531 83726.2 -43732.865 83759.533 -43766.198 83792.866 -43832.865 M 83726.2 -44332.865 83759.533 -44366.198 83792.866 -44432.865 83792.866 -44599.532 83759.533 -44666.198 83726.2 -44699.532 83659.533 -44732.865 83592.866 -44732.865 83492.866 -44699.532 83092.866 -44299.532 83092.866 -44732.865 M 83792.866 -44966.199 83792.866 -45399.532 83526.2 -45166.199 83526.2 -45266.199 83492.866 -45332.865 83459.533 -45366.199 83392.866 -45399.532 83226.2 -45399.532 83159.533 -45366.199 83126.2 -45332.865 83092.866 -45266.199 83092.866 -45066.199 83126.2 -44999.532 83159.533 -44966.199 M 78740 -69469 78740 -69215 M 80010 -69469 80010 -69215 M 78740 -69469 80010 -69469\\" fill=\\"none\\" stroke-width=\\"120\\"/><path d=\\"M 81607.428 -58547.142 81893.142 -58347.142 M 81607.428 -58204.285 82207.428 -58204.285 82207.428 -58432.856 82178.857 -58489.999 82150.285 -58518.57 82093.142 -58547.142 82007.428 -58547.142 81950.285 -58518.57 81921.714 -58489.999 81893.142 -58432.856 81893.142 -58204.285 M 81636 -58775.713 81607.428 -58861.428 81607.428 -59004.285 81636 -59061.428 81664.571 -59089.999 81721.714 -59118.57 81778.857 -59118.57 81836 -59089.999 81864.571 -59061.428 81893.142 -59004.285 81921.714 -58889.999 81950.285 -58832.856 81978.857 -58804.285 82036 -58775.713 82093.142 -58775.713 82150.285 -58804.285 82178.857 -58832.856 82207.428 -58889.999 82207.428 -59032.856 82178.857 -59118.57 M 82207.428 -59289.999 82207.428 -59632.857 M 81607.428 -59461.428 82207.428 -59461.428 M 80337.428 -58604.286 80937.428 -58604.286 80337.428 -58947.143 80937.428 -58947.143 M 80394.571 -59575.714 80366 -59547.142 80337.428 -59461.428 80337.428 -59404.285 80366 -59318.571 80423.142 -59261.428 80480.285 -59232.857 80594.571 -59204.285 80680.285 -59204.285 80794.571 -59232.857 80851.714 -59261.428 80908.857 -59318.571 80937.428 -59404.285 80937.428 -59461.428 80908.857 -59547.142 80880.285 -59575.714\\" fill=\\"none\\" stroke-width=\\"100\\"/><path d=\\"M 70707.742 -65522.999 69979.171 -65522.999 69893.457 -65565.856 69850.6 -65608.714 69807.742 -65694.428 69807.742 -65865.856 69850.6 -65951.571 69893.457 -65994.428 69979.171 -66037.285 70707.742 -66037.285 M 70064.885 -66422.999 70064.885 -66851.571 M 69807.742 -66337.285 70707.742 -66637.285 69807.742 -66937.285 M 69807.742 -67751.571 70236.314 -67451.571 M 69807.742 -67237.285 70707.742 -67237.285 70707.742 -67580.142 70664.885 -67665.857 70622.028 -67708.714 70536.314 -67751.571 70407.742 -67751.571 70322.028 -67708.714 70279.171 -67665.857 70236.314 -67580.142 70236.314 -67237.285 M 70707.742 -68008.714 70707.742 -68523 M 69807.742 -68265.857 70707.742 -68265.857 M 70150.6 -68822.999 70150.6 -69508.714 69979.171 -69337.285 M 70150.6 -69508.714 70322.028 -69337.285\\" fill=\\"none\\" stroke-width=\\"120\\"/><path d=\\"M 78368.857 -67767.142 78397.428 -67710 78397.428 -67624.285 78368.857 -67538.571 78311.714 -67481.428 78254.571 -67452.857 78140.285 -67424.285 78054.571 -67424.285 77940.285 -67452.857 77883.142 -67481.428 77826 -67538.571 77797.428 -67624.285 77797.428 -67681.428 77826 -67767.142 77854.571 -67795.714 78054.571 -67795.714 78054.571 -67681.428 M 77797.428 -68052.857 78397.428 -68052.857 77797.428 -68395.714 78397.428 -68395.714 M 77797.428 -68681.428 78397.428 -68681.428 78397.428 -68824.285 78368.857 -68909.999 78311.714 -68967.142 78254.571 -68995.713 78140.285 -69024.285 78054.571 -69024.285 77940.285 -68995.713 77883.142 -68967.142 77826 -68909.999 77797.428 -68824.285 77797.428 -68681.428 M 77127.428 -67367.142 76527.428 -67567.142 77127.428 -67767.142 M 76584.571 -68310 76556 -68281.428 76527.428 -68195.714 76527.428 -68138.571 76556 -68052.857 76613.142 -67995.714 76670.285 -67967.143 76784.571 -67938.571 76870.285 -67938.571 76984.571 -67967.143 77041.714 -67995.714 77098.857 -68052.857 77127.428 -68138.571 77127.428 -68195.714 77098.857 -68281.428 77070.285 -68310 M 76584.571 -68910 76556 -68881.428 76527.428 -68795.714 76527.428 -68738.571 76556 -68652.857 76613.142 -68595.714 76670.285 -68567.143 76784.571 -68538.571 76870.285 -68538.571 76984.571 -68567.143 77041.714 -68595.714 77098.857 -68652.857 77127.428 -68738.571 77127.428 -68795.714 77098.857 -68881.428 77070.285 -68910\\" fill=\\"none\\" stroke-width=\\"100\\"/><path d=\\"M 85107.4 -34550.333 85074.066 -34650.333 85074.066 -34817 85107.4 -34883.666 85140.733 -34917 85207.4 -34950.333 85274.066 -34950.333 85340.733 -34917 85374.066 -34883.666 85407.4 -34817 85440.733 -34683.666 85474.066 -34617 85507.4 -34583.666 85574.066 -34550.333 85640.733 -34550.333 85707.4 -34583.666 85740.733 -34617 85774.066 -34683.666 85774.066 -34850.333 85740.733 -34950.333 M 85774.066 -35183.667 85074.066 -35350.333 85574.066 -35483.667 85074.066 -35617 85774.066 -35783.667 M 85074.066 -36050.333 85774.066 -36050.333 85774.066 -36217 85740.733 -36317 85674.066 -36383.667 85607.4 -36417 85474.066 -36450.333 85374.066 -36450.333 85240.733 -36417 85174.066 -36383.667 85107.4 -36317 85074.066 -36217 85074.066 -36050.333 M 85807.4 -37250.333 84907.4 -36650.333 M 85774.066 -37483.666 85207.4 -37483.666 85140.733 -37517 85107.4 -37550.333 85074.066 -37617 85074.066 -37750.333 85107.4 -37817 85140.733 -37850.333 85207.4 -37883.666 85774.066 -37883.666 M 85274.066 -38183.666 85274.066 -38516.999 M 85074.066 -38116.999 85774.066 -38350.333 85074.066 -38583.666 M 85074.066 -39216.999 85407.4 -38983.666 M 85074.066 -38816.999 85774.066 -38816.999 85774.066 -39083.666 85740.733 -39150.333 85707.4 -39183.666 85640.733 -39216.999 85540.733 -39216.999 85474.066 -39183.666 85440.733 -39150.333 85407.4 -39083.666 85407.4 -38816.999 M 85774.066 -39416.999 85774.066 -39816.999 M 85074.066 -39616.999 85774.066 -39616.999 M 85074.066 -40883.665 85440.733 -40883.665 85507.4 -40850.332 85540.733 -40783.665 85540.733 -40650.332 85507.4 -40583.665 M 85107.4 -40883.665 85074.066 -40816.999 85074.066 -40650.332 85107.4 -40583.665 85174.066 -40550.332 85240.733 -40550.332 85307.4 -40583.665 85340.733 -40650.332 85340.733 -40816.999 85374.066 -40883.665 M 85074.066 -41516.998 85774.066 -41516.998 M 85107.4 -41516.998 85074.066 -41450.332 85074.066 -41316.998 85107.4 -41250.332 85140.733 -41216.998 85207.4 -41183.665 85407.4 -41183.665 85474.066 -41216.998 85507.4 -41250.332 85540.733 -41316.998 85540.733 -41450.332 85507.4 -41516.998 M 85074.066 -42150.331 85440.733 -42150.331 85507.4 -42116.998 85540.733 -42050.331 85540.733 -41916.998 85507.4 -41850.331 M 85107.4 -42150.331 85074.066 -42083.665 85074.066 -41916.998 85107.4 -41850.331 85174.066 -41816.998 85240.733 -41816.998 85307.4 -41850.331 85340.733 -41916.998 85340.733 -42083.665 85374.066 -42150.331 M 85540.733 -42483.664 84840.733 -42483.664 M 85507.4 -42483.664 85540.733 -42550.331 85540.733 -42683.664 85507.4 -42750.331 85474.066 -42783.664 85407.4 -42816.998 85207.4 -42816.998 85140.733 -42783.664 85107.4 -42750.331 85074.066 -42683.664 85074.066 -42550.331 85107.4 -42483.664 M 85540.733 -43016.997 85540.733 -43283.664 M 85774.066 -43116.997 85174.066 -43116.997 85107.4 -43150.331 85074.066 -43216.997 85074.066 -43283.664 M 85107.4 -43783.664 85074.066 -43716.997 85074.066 -43583.664 85107.4 -43516.997 85174.066 -43483.664 85440.733 -43483.664 85507.4 -43516.997 85540.733 -43583.664 85540.733 -43716.997 85507.4 -43783.664 85440.733 -43816.997 85374.066 -43816.997 85307.4 -43483.664 M 85074.066 -44116.997 85540.733 -44116.997 M 85407.4 -44116.997 85474.066 -44150.331 85507.4 -44183.664 85540.733 -44250.331 85540.733 -44316.997\\" fill=\\"none\\" stroke-width=\\"120\\"/><path d=\\"M 77854.571 -58490 77826 -58461.428 77797.428 -58375.714 77797.428 -58318.571 77826 -58232.857 77883.142 -58175.714 77940.285 -58147.143 78054.571 -58118.571 78140.285 -58118.571 78254.571 -58147.143 78311.714 -58175.714 78368.857 -58232.857 78397.428 -58318.571 78397.428 -58375.714 78368.857 -58461.428 78340.285 -58490 M 77797.428 -59032.857 77797.428 -58747.143 78397.428 -58747.143 M 77797.428 -59232.857 78397.428 -59232.857 M 77797.428 -59575.714 78140.285 -59318.571 M 78397.428 -59575.714 78054.571 -59232.857 M 79638.857 -67767.142 79667.428 -67710 79667.428 -67624.285 79638.857 -67538.571 79581.714 -67481.428 79524.571 -67452.857 79410.285 -67424.285 79324.571 -67424.285 79210.285 -67452.857 79153.142 -67481.428 79096 -67538.571 79067.428 -67624.285 79067.428 -67681.428 79096 -67767.142 79124.571 -67795.714 79324.571 -67795.714 79324.571 -67681.428 M 79067.428 -68052.857 79667.428 -68052.857 79067.428 -68395.714 79667.428 -68395.714 M 79067.428 -68681.428 79667.428 -68681.428 79667.428 -68824.285 79638.857 -68909.999 79581.714 -68967.142 79524.571 -68995.713 79410.285 -69024.285 79324.571 -69024.285 79210.285 -68995.713 79153.142 -68967.142 79096 -68909.999 79067.428 -68824.285 79067.428 -68681.428 M 63157.428 -58718.57 63157.428 -59061.428 M 62557.428 -58889.999 63157.428 -58889.999 M 63157.428 -59204.285 62557.428 -59604.285 M 63157.428 -59604.285 62557.428 -59204.285\\" fill=\\"none\\" stroke-width=\\"100\\"/><path d=\\"M 66394 -43156.333 66860.666 -43856.333 M 66860.666 -43156.333 66394 -43856.333 M 67127.333 -43856.333 67127.333 -43156.333 M 67427.333 -43656.333 67760.666 -43656.333 M 67360.666 -43856.333 67594 -43156.333 67827.333 -43856.333 M 68194 -43156.333 68327.333 -43156.333 68394 -43189.666 68460.666 -43256.333 68494 -43389.666 68494 -43623 68460.666 -43756.333 68394 -43823 68327.333 -43856.333 68194 -43856.333 68127.333 -43823 68060.666 -43756.333 68027.333 -43623 68027.333 -43389.666 68060.666 -43256.333 68127.333 -43189.666 68194 -43156.333 M 69727.332 -43856.333 69493.999 -43523 M 69327.332 -43856.333 69327.332 -43156.333 69593.999 -43156.333 69660.666 -43189.666 69693.999 -43223 69727.332 -43289.666 69727.332 -43389.666 69693.999 -43456.333 69660.666 -43489.666 69593.999 -43523 69327.332 -43523 M 70027.332 -43856.333 70027.332 -43156.333 70293.999 -43156.333 70360.666 -43189.666 70393.999 -43223 70427.332 -43289.666 70427.332 -43389.666 70393.999 -43456.333 70360.666 -43489.666 70293.999 -43523 70027.332 -43523 M 70693.999 -43223 70727.332 -43189.666 70793.999 -43156.333 70960.666 -43156.333 71027.332 -43189.666 71060.666 -43223 71093.999 -43289.666 71093.999 -43356.333 71060.666 -43456.333 70660.666 -43856.333 71093.999 -43856.333 M 71527.333 -43156.333 71593.999 -43156.333 71660.666 -43189.666 71693.999 -43223 71727.333 -43289.666 71760.666 -43423 71760.666 -43589.666 71727.333 -43723 71693.999 -43789.666 71660.666 -43823 71593.999 -43856.333 71527.333 -43856.333 71460.666 -43823 71427.333 -43789.666 71393.999 -43723 71360.666 -43589.666 71360.666 -43423 71393.999 -43289.666 71427.333 -43223 71460.666 -43189.666 71527.333 -43156.333 M 72360.666 -43389.666 72360.666 -43856.333 M 72194 -43123 72027.333 -43623 72460.666 -43623 M 72860.667 -43156.333 72927.333 -43156.333 72994 -43189.666 73027.333 -43223 73060.667 -43289.666 73094 -43423 73094 -43589.666 73060.667 -43723 73027.333 -43789.666 72994 -43823 72927.333 -43856.333 72860.667 -43856.333 72794 -43823 72760.667 -43789.666 72727.333 -43723 72694 -43589.666 72694 -43423 72727.333 -43289.666 72760.667 -43223 72794 -43189.666 72860.667 -43156.333\\" fill=\\"none\\" stroke-width=\\"120\\"/><path d=\\"M 55508.857 -58318.571 55537.428 -58261.429 55537.428 -58175.714 55508.857 -58090 55451.714 -58032.857 55394.571 -58004.286 55280.285 -57975.714 55194.571 -57975.714 55080.285 -58004.286 55023.142 -58032.857 54966 -58090 54937.428 -58175.714 54937.428 -58232.857 54966 -58318.571 54994.571 -58347.143 55194.571 -58347.143 55194.571 -58232.857 M 54937.428 -58604.286 55537.428 -58604.286 54937.428 -58947.143 55537.428 -58947.143 M 54937.428 -59232.857 55537.428 -59232.857 55537.428 -59375.714 55508.857 -59461.428 55451.714 -59518.571 55394.571 -59547.142 55280.285 -59575.714 55194.571 -59575.714 55080.285 -59547.142 55023.142 -59518.571 54966 -59461.428 54937.428 -59375.714 54937.428 -59232.857\\" fill=\\"none\\" stroke-width=\\"100\\"/><path d=\\"M 72924 -61721.428 72881.142 -61850 72881.142 -62064.285 72924 -62150 72966.857 -62192.857 73052.571 -62235.714 73138.285 -62235.714 73224 -62192.857 73266.857 -62150 73309.714 -62064.285 73352.571 -61892.857 73395.428 -61807.142 73438.285 -61764.285 73524 -61721.428 73609.714 -61721.428 73695.428 -61764.285 73738.285 -61807.142 73781.142 -61892.857 73781.142 -62107.142 73738.285 -62235.714 M 73781.142 -62535.714 72881.142 -62750 73524 -62921.428 72881.142 -63092.857 73781.142 -63307.143 M 72881.142 -63649.999 73781.142 -63649.999 73781.142 -63864.285 73738.285 -63992.856 73652.571 -64078.571 73566.857 -64121.428 73395.428 -64164.285 73266.857 -64164.285 73095.428 -64121.428 73009.714 -64078.571 72924 -63992.856 72881.142 -63864.285 72881.142 -63649.999 M 73224 -64549.999 73224 -65235.714 73052.571 -65064.285 M 73224 -65235.714 73395.428 -65064.285\\" fill=\\"none\\" stroke-width=\\"120\\"/><path d=\\"M 65097.428 -59004.285 65383.142 -58804.285 M 65097.428 -58661.428 65697.428 -58661.428 65697.428 -58889.999 65668.857 -58947.142 65640.285 -58975.713 65583.142 -59004.285 65497.428 -59004.285 65440.285 -58975.713 65411.714 -58947.142 65383.142 -58889.999 65383.142 -58661.428 M 65697.428 -59204.285 65097.428 -59604.285 M 65697.428 -59604.285 65097.428 -59204.285 M 79096 -57918.571 79067.428 -58004.286 79067.428 -58147.143 79096 -58204.286 79124.571 -58232.857 79181.714 -58261.428 79238.857 -58261.428 79296 -58232.857 79324.571 -58204.286 79353.142 -58147.143 79381.714 -58032.857 79410.285 -57975.714 79438.857 -57947.143 79496 -57918.571 79553.142 -57918.571 79610.285 -57947.143 79638.857 -57975.714 79667.428 -58032.857 79667.428 -58175.714 79638.857 -58261.428 M 79667.428 -58461.429 79067.428 -58604.286 79496 -58718.572 79067.428 -58832.857 79667.428 -58975.715 M 79667.428 -59318.571 79667.428 -59432.857 79638.857 -59490 79581.714 -59547.143 79467.428 -59575.714 79267.428 -59575.714 79153.142 -59547.143 79096 -59490 79067.428 -59432.857 79067.428 -59318.571 79096 -59261.429 79153.142 -59204.286 79267.428 -59175.714 79467.428 -59175.714 79581.714 -59204.286 79638.857 -59261.429 79667.428 -59318.571 M 82178.857 -67767.142 82207.428 -67710 82207.428 -67624.285 82178.857 -67538.571 82121.714 -67481.428 82064.571 -67452.857 81950.285 -67424.285 81864.571 -67424.285 81750.285 -67452.857 81693.142 -67481.428 81636 -67538.571 81607.428 -67624.285 81607.428 -67681.428 81636 -67767.142 81664.571 -67795.714 81864.571 -67795.714 81864.571 -67681.428 M 81607.428 -68052.857 82207.428 -68052.857 81607.428 -68395.714 82207.428 -68395.714 M 81607.428 -68681.428 82207.428 -68681.428 82207.428 -68824.285 82178.857 -68909.999 82121.714 -68967.142 82064.571 -68995.713 81950.285 -69024.285 81864.571 -69024.285 81750.285 -68995.713 81693.142 -68967.142 81636 -68909.999 81607.428 -68824.285 81607.428 -68681.428 M 76527.428 -58318.572 77127.428 -58318.572 77127.428 -58461.429 77098.857 -58547.143 77041.714 -58604.286 76984.571 -58632.857 76870.285 -58661.429 76784.571 -58661.429 76670.285 -58632.857 76613.142 -58604.286 76556 -58547.143 76527.428 -58461.429 76527.428 -58318.572 M 76527.428 -58918.572 77127.428 -58918.572 M 77127.428 -59318.571 77127.428 -59432.857 77098.857 -59490 77041.714 -59547.143 76927.428 -59575.714 76727.428 -59575.714 76613.142 -59547.143 76556 -59490 76527.428 -59432.857 76527.428 -59318.571 76556 -59261.429 76613.142 -59204.286 76727.428 -59175.714 76927.428 -59175.714 77041.714 -59204.286 77098.857 -59261.429 77127.428 -59318.571 M 67637.428 -58547.142 67923.142 -58347.142 M 67637.428 -58204.285 68237.428 -58204.285 68237.428 -58432.856 68208.857 -58489.999 68180.285 -58518.57 68123.142 -58547.142 68037.428 -58547.142 67980.285 -58518.57 67951.714 -58489.999 67923.142 -58432.856 67923.142 -58204.285 M 68237.428 -58718.57 68237.428 -59061.428 M 67637.428 -58889.999 68237.428 -58889.999 M 67666 -59232.856 67637.428 -59318.571 67637.428 -59461.428 67666 -59518.571 67694.571 -59547.142 67751.714 -59575.713 67808.857 -59575.713 67866 -59547.142 67894.571 -59518.571 67923.142 -59461.428 67951.714 -59347.142 67980.285 -59289.999 68008.857 -59261.428 68066 -59232.856 68123.142 -59232.856 68180.285 -59261.428 68208.857 -59289.999 68237.428 -59347.142 68237.428 -59489.999 68208.857 -59575.713 M 60617.428 -59032.856 60617.428 -58747.142 60331.714 -58718.57 60360.285 -58747.142 60388.857 -58804.285 60388.857 -58947.142 60360.285 -59004.285 60331.714 -59032.856 60274.571 -59061.427 60131.714 -59061.427 60074.571 -59032.856 60046 -59004.285 60017.428 -58947.142 60017.428 -58804.285 60046 -58747.142 60074.571 -58718.57 M 60617.428 -59232.856 60017.428 -59432.856 60617.428 -59632.856 M 57534.571 -58547.142 57506 -58518.57 57477.428 -58432.856 57477.428 -58375.713 57506 -58289.999 57563.142 -58232.856 57620.285 -58204.285 57734.571 -58175.713 57820.285 -58175.713 57934.571 -58204.285 57991.714 -58232.856 58048.857 -58289.999 58077.428 -58375.713 58077.428 -58432.856 58048.857 -58518.57 58020.285 -58547.142 M 58077.428 -58718.57 58077.428 -59061.428 M 57477.428 -58889.999 58077.428 -58889.999 M 57506 -59232.856 57477.428 -59318.571 57477.428 -59461.428 57506 -59518.571 57534.571 -59547.142 57591.714 -59575.713 57648.857 -59575.713 57706 -59547.142 57734.571 -59518.571 57763.142 -59461.428 57791.714 -59347.142 57820.285 -59289.999 57848.857 -59261.428 57906 -59232.856 57963.142 -59232.856 58020.285 -59261.428 58048.857 -59289.999 58077.428 -59347.142 58077.428 -59489.999 58048.857 -59575.713 M 80337.428 -67452.857 80937.428 -67452.857 M 80337.428 -67795.714 80680.285 -67538.571 M 80937.428 -67795.714 80594.571 -67452.857 M 80651.714 -68052.857 80651.714 -68252.857 M 80337.428 -68338.571 80337.428 -68052.857 80937.428 -68052.857 80937.428 -68338.571 M 80623.142 -68709.999 80337.428 -68709.999 M 80937.428 -68509.999 80623.142 -68709.999 80937.428 -68909.999 M 61326.666 -65663.833 61326.666 -66163.833 61293.333 -66263.833 61226.666 -66330.5 61126.666 -66363.833 61060 -66363.833 M 61626.666 -65730.5 61659.999 -65697.166 61726.666 -65663.833 61893.333 -65663.833 61959.999 -65697.166 61993.333 -65730.5 62026.666 -65797.166 62026.666 -65863.833 61993.333 -65963.833 61593.333 -66363.833 62026.666 -66363.833 M 84338.333 -48583.833 84105 -48250.5 M 83938.333 -48583.833 83938.333 -47883.833 84205 -47883.833 84271.667 -47917.166 84305 -47950.5 84338.333 -48017.166 84338.333 -48117.166 84305 -48183.833 84271.667 -48217.166 84205 -48250.5 83938.333 -48250.5 M 84571.667 -47883.833 85005 -47883.833 84771.667 -48150.5 84871.667 -48150.5 84938.333 -48183.833 84971.667 -48217.166 85005 -48283.833 85005 -48450.5 84971.667 -48517.166 84938.333 -48550.5 84871.667 -48583.833 84671.667 -48583.833 84605 -48550.5 84571.667 -48517.166 M 84371.666 -53663.833 83971.666 -53663.833 M 84171.666 -53663.833 84171.666 -52963.833 84104.999 -53063.833 84038.333 -53130.5 83971.666 -53163.833 M 84671.666 -53663.833 84671.666 -52963.833 M 84738.333 -53397.166 84938.333 -53663.833 M 84938.333 -53197.166 84671.666 -53463.833 M 82947.733 -55645.033 82947.733 -54945.033 83114.4 -54945.033 83214.4 -54978.366 83281.067 -55045.033 83314.4 -55111.7 83347.733 -55245.033 83347.733 -55345.033 83314.4 -55478.366 83281.067 -55545.033 83214.4 -55611.7 83114.4 -55645.033 82947.733 -55645.033 M 83614.4 -55011.7 83647.733 -54978.366 83714.4 -54945.033 83881.067 -54945.033 83947.733 -54978.366 83981.067 -55011.7 84014.4 -55078.366 84014.4 -55145.033 83981.067 -55245.033 83581.067 -55645.033 84014.4 -55645.033 M 56938.133 -55594.233 56938.133 -54894.233 57104.8 -54894.233 57204.8 -54927.566 57271.467 -54994.233 57304.8 -55060.9 57338.133 -55194.233 57338.133 -55294.233 57304.8 -55427.566 57271.467 -55494.233 57204.8 -55560.9 57104.8 -55594.233 56938.133 -55594.233 M 58004.8 -55594.233 57604.8 -55594.233 M 57804.8 -55594.233 57804.8 -54894.233 57738.133 -54994.233 57671.467 -55060.9 57604.8 -55094.233 M 73543.333 -55568.833 73310 -55235.5 M 73143.333 -55568.833 73143.333 -54868.833 73410 -54868.833 73476.667 -54902.166 73510 -54935.5 73543.333 -55002.166 73543.333 -55102.166 73510 -55168.833 73476.667 -55202.166 73410 -55235.5 73143.333 -55235.5 M 73810 -54935.5 73843.333 -54902.166 73910 -54868.833 74076.667 -54868.833 74143.333 -54902.166 74176.667 -54935.5 74210 -55002.166 74210 -55068.833 74176.667 -55168.833 73776.667 -55568.833 74210 -55568.833 M 73576.666 -56838.833 73176.666 -56838.833 M 73376.666 -56838.833 73376.666 -56138.833 73309.999 -56238.833 73243.333 -56305.5 73176.666 -56338.833 M 73876.666 -56838.833 73876.666 -56138.833 M 73943.333 -56572.166 74143.333 -56838.833 M 74143.333 -56372.166 73876.666 -56638.833 M 66721.5 -33484.476 66697.69 -33555.904 66697.69 -33674.952 66721.5 -33722.571 66745.309 -33746.38 66792.928 -33770.19 66840.547 -33770.19 66888.166 -33746.38 66911.976 -33722.571 66935.785 -33674.952 66959.595 -33579.714 66983.404 -33532.095 67007.214 -33508.285 67054.833 -33484.476 67102.452 -33484.476 67150.071 -33508.285 67173.88 -33532.095 67197.69 -33579.714 67197.69 -33698.761 67173.88 -33770.19 M 67197.69 -33936.856 66697.69 -34055.904 67054.833 -34151.142 66697.69 -34246.38 67197.69 -34365.428 M 66697.69 -34555.904 67197.69 -34555.904 67197.69 -34674.952 67173.88 -34746.38 67126.261 -34793.999 67078.642 -34817.809 66983.404 -34841.618 66911.976 -34841.618 66816.738 -34817.809 66769.119 -34793.999 66721.5 -34746.38 66697.69 -34674.952 66697.69 -34555.904 M 66697.69 -35055.904 67197.69 -35055.904 M 67197.69 -35389.237 67197.69 -35484.475 67173.88 -35532.094 67126.261 -35579.713 67031.023 -35603.523 66864.357 -35603.523 66769.119 -35579.713 66721.5 -35532.094 66697.69 -35484.475 66697.69 -35389.237 66721.5 -35341.618 66769.119 -35293.999 66864.357 -35270.19 67031.023 -35270.19 67126.261 -35293.999 67173.88 -35341.618 67197.69 -35389.237 M 72639.88 -51689.047 72663.69 -51641.428 72663.69 -51569.999 72639.88 -51498.571 72592.261 -51450.952 72544.642 -51427.142 72449.404 -51403.333 72377.976 -51403.333 72282.738 -51427.142 72235.119 -51450.952 72187.5 -51498.571 72163.69 -51569.999 72163.69 -51617.618 72187.5 -51689.047 72211.309 -51712.856 72377.976 -51712.856 72377.976 -51617.618 M 72163.69 -51927.142 72663.69 -51927.142 72163.69 -52212.856 72663.69 -52212.856 M 72163.69 -52450.952 72663.69 -52450.952 72663.69 -52570 72639.88 -52641.428 72592.261 -52689.047 72544.642 -52712.857 72449.404 -52736.666 72377.976 -52736.666 72282.738 -52712.857 72235.119 -52689.047 72187.5 -52641.428 72163.69 -52570 72163.69 -52450.952 M 67583.69 -51522.381 67083.69 -51689.047 67583.69 -51855.714 M 67083.69 -52022.38 67583.69 -52022.38 M 67083.69 -52260.475 67583.69 -52260.475 67083.69 -52546.189 67583.69 -52546.189 M 66697.69 -36484.809 66935.785 -36318.143 M 66697.69 -36199.095 67197.69 -36199.095 67197.69 -36389.571 67173.88 -36437.19 67150.071 -36461 67102.452 -36484.809 67031.023 -36484.809 66983.404 -36461 66959.595 -36437.19 66935.785 -36389.571 66935.785 -36199.095 M 66959.595 -36699.095 66959.595 -36865.762 M 66697.69 -36937.19 66697.69 -36699.095 67197.69 -36699.095 67197.69 -36937.19 M 66721.5 -37127.667 66697.69 -37199.095 66697.69 -37318.143 66721.5 -37365.762 66745.309 -37389.571 66792.928 -37413.381 66840.547 -37413.381 66888.166 -37389.571 66911.976 -37365.762 66935.785 -37318.143 66959.595 -37222.905 66983.404 -37175.286 67007.214 -37151.476 67054.833 -37127.667 67102.452 -37127.667 67150.071 -37151.476 67173.88 -37175.286 67197.69 -37222.905 67197.69 -37341.952 67173.88 -37413.381 M 66959.595 -37627.666 66959.595 -37794.333 M 66697.69 -37865.761 66697.69 -37627.666 67197.69 -37627.666 67197.69 -37865.761 M 67197.69 -38008.619 67197.69 -38294.333 M 66697.69 -38151.476 67197.69 -38151.476 M 72436.5 -33540.048 72412.69 -33611.476 72412.69 -33730.524 72436.5 -33778.143 72460.309 -33801.952 72507.928 -33825.762 72555.547 -33825.762 72603.166 -33801.952 72626.976 -33778.143 72650.785 -33730.524 72674.595 -33635.286 72698.404 -33587.667 72722.214 -33563.857 72769.833 -33540.048 72817.452 -33540.048 72865.071 -33563.857 72888.88 -33587.667 72912.69 -33635.286 72912.69 -33754.333 72888.88 -33825.762 M 72912.69 -33992.428 72412.69 -34111.476 72769.833 -34206.714 72412.69 -34301.952 72912.69 -34421 M 72460.309 -34897.19 72436.5 -34873.381 72412.69 -34801.952 72412.69 -34754.333 72436.5 -34682.905 72484.119 -34635.286 72531.738 -34611.476 72626.976 -34587.667 72698.404 -34587.667 72793.642 -34611.476 72841.261 -34635.286 72888.88 -34682.905 72912.69 -34754.333 72912.69 -34801.952 72888.88 -34873.381 72865.071 -34897.19 M 72412.69 -35349.571 72412.69 -35111.476 72912.69 -35111.476 M 72412.69 -35516.238 72912.69 -35516.238 M 72412.69 -35801.952 72698.404 -35587.667 M 72912.69 -35801.952 72626.976 -35516.238 M 72888.88 -36957.047 72912.69 -36909.428 72912.69 -36837.999 72888.88 -36766.571 72841.261 -36718.952 72793.642 -36695.142 72698.404 -36671.333 72626.976 -36671.333 72531.738 -36695.142 72484.119 -36718.952 72436.5 -36766.571 72412.69 -36837.999 72412.69 -36885.618 72436.5 -36957.047 72460.309 -36980.856 72626.976 -36980.856 72626.976 -36885.618 M 72412.69 -37195.142 72912.69 -37195.142 72412.69 -37480.856 72912.69 -37480.856 M 72412.69 -37718.952 72912.69 -37718.952 72912.69 -37838 72888.88 -37909.428 72841.261 -37957.047 72793.642 -37980.857 72698.404 -38004.666 72626.976 -38004.666 72531.738 -37980.857 72484.119 -37957.047 72436.5 -37909.428 72412.69 -37838 72412.69 -37718.952 M 55763.333 -43503.833 55530 -43170.5 M 55363.333 -43503.833 55363.333 -42803.833 55630 -42803.833 55696.667 -42837.166 55730 -42870.5 55763.333 -42937.166 55763.333 -43037.166 55730 -43103.833 55696.667 -43137.166 55630 -43170.5 55363.333 -43170.5 M 56430 -43503.833 56030 -43503.833 M 56230 -43503.833 56230 -42803.833 56163.333 -42903.833 56096.667 -42970.5 56030 -43003.833 M 55695.066 -48710.833 55295.066 -48710.833 M 55495.066 -48710.833 55495.066 -48010.833 55428.399 -48110.833 55361.733 -48177.5 55295.066 -48210.833 M 55995.066 -48710.833 55995.066 -48010.833 M 56061.733 -48444.166 56261.733 -48710.833 M 56261.733 -48244.166 55995.066 -48510.833 M 79141.666 -63123.833 79141.666 -63623.833 79108.333 -63723.833 79041.666 -63790.5 78941.666 -63823.833 78875 -63823.833 M 79841.666 -63823.833 79441.666 -63823.833 M 79641.666 -63823.833 79641.666 -63123.833 79574.999 -63223.833 79508.333 -63290.5 79441.666 -63323.833\\" fill=\\"none\\" stroke-width=\\"100\\"/><path d=\\"M 69240 -60280 69240 -61550 M 68224 -63328 67716 -63328 67716 -64090 M 65684 -63328 65176 -63328 65176 -64090 M 63144 -63328 62636 -63328 62636 -64090 M 60604 -63328 60096 -63328 60096 -64090 M 58064 -63328 57556 -63328 57556 -64090 M 55524 -63328 55016 -63328 55016 -64090 M 68224 -64090 68224 -63328 M 65684 -64090 65684 -63328 M 63144 -64090 63144 -63328 M 60604 -64090 60604 -63328 M 58064 -64090 58064 -63328 M 55524 -64090 55524 -63328 M 69240 -64090 54000 -64090 54000 -72980 69240 -72980 69240 -64090 M 83620 -49800 83620 -51800 M 81480 -51800 81480 -49800 M 82475 -56830 82475 -54930 77775 -54930 M 82475 -56830 77775 -56830 M 64695 -56830 64695 -54930 59995 -54930 M 64695 -56830 59995 -56830 M 70215 -56950 68215 -56950 M 68215 -54810 70215 -54810 M 60962.5 -52461 60962.5 -33919 A 1269.999 1269.999 0 0 0 62232.5 -32649 L 77472.5 -32649 A 1270 1270 0 0 0 78742.5 -33919 L 78742.5 -52461 A 1270 1270 0 0 0 77472.5 -53731 L 62232.5 -53731 A 1269.999 1269.999 0 0 0 60962.5 -52461 M 65352.5 -32649 74352.5 -32649 74352.5 -31125 65352.5 -31125 65352.5 -32649 M 56715 -46720 56715 -44720 M 58855 -44720 58855 -46720 M 82545 -60410 82545 -61735 84455 -61735 84455 -65265 82545 -65265 M 76205 -61735 74295 -61735 74295 -65265 76205 -65265\\" fill=\\"none\\" stroke-width=\\"120\\"/></g></svg>"]]');

updateSVG();
